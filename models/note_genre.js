import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class NoteGenre extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    note_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'notes',
        key: 'id'
      }
    },
    genre_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'genres',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'note_genres',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "note_id" },
          { name: "genre_id" },
        ]
      },
      {
        name: "note_id",
        using: "BTREE",
        fields: [
          { name: "note_id" },
        ]
      },
      {
        name: "genre_id",
        using: "BTREE",
        fields: [
          { name: "genre_id" },
        ]
      },
    ]
  });
  return NoteGenre;
  }
}

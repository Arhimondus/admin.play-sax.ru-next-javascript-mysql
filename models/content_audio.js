import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class ContentAudio extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    content_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'contents',
        key: 'id'
      }
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    file_name: {
      type: DataTypes.STRING(22),
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize,
    tableName: 'content_audios',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "content_id" },
          { name: "file_name" },
        ]
      },
    ]
  });
  return ContentAudio;
  }
}

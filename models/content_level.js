import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class ContentLevel extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    content_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'contents',
        key: 'id'
      }
    },
    level_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'levels',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'content_levels',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "content_id" },
          { name: "level_id" },
        ]
      },
      {
        name: "content_id",
        using: "BTREE",
        fields: [
          { name: "content_id" },
        ]
      },
      {
        name: "level_id",
        using: "BTREE",
        fields: [
          { name: "level_id" },
        ]
      },
    ]
  });
  return ContentLevel;
  }
}

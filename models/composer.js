import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class Composer extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    imya: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    familiya: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    otchestvo: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'composers',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return Composer;
  }
}

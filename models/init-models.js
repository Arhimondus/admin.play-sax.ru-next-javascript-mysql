import _sequelize from "sequelize";
const DataTypes = _sequelize.DataTypes;
import _Article from  "./article.js";
import _Composer from  "./composer.js";
import _ContentAudio from  "./content_audio.js";
import _ContentInstrument from  "./content_instrument.js";
import _ContentLevel from  "./content_level.js";
import _ContentScore from  "./content_score.js";
import _Content from  "./content.js";
import _Genre from  "./genre.js";
import _Instrument from  "./instrument.js";
import _Level from  "./level.js";
import _NoteComposer from  "./note_composer.js";
import _NoteGenre from  "./note_genre.js";
import _Note from  "./note.js";
import _StaticPage from  "./static_page.js";

export default function initModels(sequelize) {
  var Article = _Article.init(sequelize, DataTypes);
  var Composer = _Composer.init(sequelize, DataTypes);
  var ContentAudio = _ContentAudio.init(sequelize, DataTypes);
  var ContentInstrument = _ContentInstrument.init(sequelize, DataTypes);
  var ContentLevel = _ContentLevel.init(sequelize, DataTypes);
  var ContentScore = _ContentScore.init(sequelize, DataTypes);
  var Content = _Content.init(sequelize, DataTypes);
  var Genre = _Genre.init(sequelize, DataTypes);
  var Instrument = _Instrument.init(sequelize, DataTypes);
  var Level = _Level.init(sequelize, DataTypes);
  var NoteComposer = _NoteComposer.init(sequelize, DataTypes);
  var NoteGenre = _NoteGenre.init(sequelize, DataTypes);
  var Note = _Note.init(sequelize, DataTypes);
  var StaticPage = _StaticPage.init(sequelize, DataTypes);

  Composer.belongsToMany(Note, { as: 'notes', through: NoteComposer, foreignKey: "composer_id", otherKey: "note_id" });
  Content.belongsToMany(Instrument, { as: 'instruments', through: ContentInstrument, foreignKey: "content_id", otherKey: "instrument_id" });
  Content.belongsToMany(Level, { as: 'levels', through: ContentLevel, foreignKey: "content_id", otherKey: "level_id" });
  Genre.belongsToMany(Note, { as: 'notes', through: NoteGenre, foreignKey: "genre_id", otherKey: "note_id" });
  Instrument.belongsToMany(Content, { as: 'contents', through: ContentInstrument, foreignKey: "instrument_id", otherKey: "content_id" });
  Level.belongsToMany(Content, { as: 'contents', through: ContentLevel, foreignKey: "level_id", otherKey: "content_id" });
  Note.belongsToMany(Composer, { as: 'composers', through: NoteComposer, foreignKey: "note_id", otherKey: "composer_id" });
  Note.belongsToMany(Genre, { as: 'genres', through: NoteGenre, foreignKey: "note_id", otherKey: "genre_id" });
  NoteComposer.belongsTo(Composer, { as: "composer", foreignKey: "composer_id"});
  Composer.hasMany(NoteComposer, { as: "note_composers", foreignKey: "composer_id"});
  ContentAudio.belongsTo(Content, { as: "content", foreignKey: "content_id"});
  Content.hasMany(ContentAudio, { as: "audios", foreignKey: "content_id"});
  ContentInstrument.belongsTo(Content, { as: "content", foreignKey: "content_id"});
  Content.hasMany(ContentInstrument, { as: "content_instruments", foreignKey: "content_id"});
  ContentLevel.belongsTo(Content, { as: "content", foreignKey: "content_id"});
  Content.hasMany(ContentLevel, { as: "content_levels", foreignKey: "content_id"});
  ContentScore.belongsTo(Content, { as: "content", foreignKey: "content_id"});
  Content.hasMany(ContentScore, { as: "scores", foreignKey: "content_id"});
  NoteGenre.belongsTo(Genre, { as: "genre", foreignKey: "genre_id"});
  Genre.hasMany(NoteGenre, { as: "note_genres", foreignKey: "genre_id"});
  ContentInstrument.belongsTo(Instrument, { as: "instrument", foreignKey: "instrument_id"});
  Instrument.hasMany(ContentInstrument, { as: "content_instruments", foreignKey: "instrument_id"});
  ContentLevel.belongsTo(Level, { as: "level", foreignKey: "level_id"});
  Level.hasMany(ContentLevel, { as: "content_levels", foreignKey: "level_id"});
  Content.belongsTo(Note, { as: "note", foreignKey: "note_id"});
  Note.hasMany(Content, { as: "contents", foreignKey: "note_id"});
  NoteComposer.belongsTo(Note, { as: "note", foreignKey: "note_id"});
  Note.hasMany(NoteComposer, { as: "note_composers", foreignKey: "note_id"});
  NoteGenre.belongsTo(Note, { as: "note", foreignKey: "note_id"});
  Note.hasMany(NoteGenre, { as: "note_genres", foreignKey: "note_id"});

  return {
    Article,
    Composer,
    ContentAudio,
    ContentInstrument,
    ContentLevel,
    ContentScore,
    Content,
    Genre,
    Instrument,
    Level,
    NoteComposer,
    NoteGenre,
    Note,
    StaticPage,
  };
}

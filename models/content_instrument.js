import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class ContentInstrument extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    content_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'contents',
        key: 'id'
      }
    },
    instrument_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'instruments',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'content_instruments',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "content_id" },
          { name: "instrument_id" },
        ]
      },
      {
        name: "instrument_id",
        using: "BTREE",
        fields: [
          { name: "instrument_id" },
        ]
      },
      {
        name: "content_id",
        using: "BTREE",
        fields: [
          { name: "content_id" },
        ]
      },
    ]
  });
  return ContentInstrument;
  }
}

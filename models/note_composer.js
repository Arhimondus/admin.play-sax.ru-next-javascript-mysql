import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class NoteComposer extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    note_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'notes',
        key: 'id'
      }
    },
    composer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'composers',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'note_composers',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "note_id" },
          { name: "composer_id" },
        ]
      },
      {
        name: "note_id",
        using: "BTREE",
        fields: [
          { name: "note_id" },
        ]
      },
      {
        name: "composer_id",
        using: "BTREE",
        fields: [
          { name: "composer_id" },
        ]
      },
    ]
  });
  return NoteComposer;
  }
}

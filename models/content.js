import _sequelize from 'sequelize';
const { Model, Sequelize } = _sequelize;

export default class Content extends Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    public: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: true
    },
    note_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'notes',
        key: 'id'
      }
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    information: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    seo_title: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    seo_description: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    pages_count: {
      type: DataTypes.SMALLINT,
      allowNull: true,
      defaultValue: 0
    },
    download_link: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    download_file: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    music_xml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    audio_file: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    video_file: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    youtube_link: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    youtube_embed: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'contents',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "note_id",
        using: "BTREE",
        fields: [
          { name: "note_id" },
        ]
      },
    ]
  });
  return Content;
  }
}

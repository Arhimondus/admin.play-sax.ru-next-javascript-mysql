import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'lib/axios';
import MultiSelect from 'react-multi-select-component';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PlaySelect from 'lib/play-select';
import { getComposerShortName } from 'lib/utils';
import { access } from 'lib/control';

export let getServerSideProps = access(user => user.admin, async ({ req, res, query: { note_id } }) => {
	const defaultPageNumber = 1;
	const defaultPageSize = 20;
	const defaultSortType = 'composer';
	
	const { data: note } = await axios.get(`/api/note`, {
		params: { note_id },
	});

	const { data: allComposers } = await axios.get(`/api/composers`);

	const { data: allGenres } = await axios.get(`/api/genres`);

	allComposers.forEach(it => {
		it.shortName = getComposerShortName(it);
	});

	note.composers.forEach(it => {
		it.shortName = getComposerShortName(it);
	});

	return {
		props: { note, allComposers, allGenres },
	};
}, '/auth');


export default function Note({ note, allComposers, allGenres }) {
	const [title, setTitle] = useState(note.title);
	const [description, setDescription] = useState(note.description);
	
	const [seoTitle, setSeoTitle] = useState(note.seo_title);
	const [seoDescription, setSeoDescription] = useState(note.seo_description);
	const [url, setUrl] = useState(note.url);
	
	const [composers, setComposers] = useState(note.composers.map(it => it.id));
	const [genres, setGenres] = useState(note.genres.map(it => it.id));
	
	function handleChange(setter) {
		return (event) => {
			setter(event.target.value);
		};
	}
	
	async function save(evt) {
		console.log('save1');
		evt.preventDefault();
		try {
			await axios.post(`/api/save-note`, {
				id: note.id,
				title,
				description,
				
				seo_title: seoTitle,
				seo_description: seoDescription,
				url,
				composers,
				genres,
			});
			toast.success('Изменения сохранены!', {
				position: 'bottom-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
			});
		} catch({ response: { data } }) {
			toast.error(`Ошибка: ${data.message ? data.message : data.toString()}`, {
				position: 'bottom-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
			});
		}
	}

	return (
		<div className="main">	
			<nav>
				<ul className="menu">
					<li><Link href="/"><a>На главную</a></Link></li>
				</ul>
			</nav>
			<header className="header">
				<h1 className="page-title">{note.title} <span className="page-title__sub">{note.composers.map(getComposerShortName).join(', ')} </span></h1>
				<div className="content-id">note id: {note.id}</div>
			</header>
			<form className="form">
				<label className="form__item">
					Название (title)
					<input type="text" value={title} onChange={handleChange(setTitle)}/>
				</label>
				<div className="form__item">
					Композиторы (> composers)
					<PlaySelect allData={allComposers} values={composers} setValues={setComposers} dataFieldLabel="shortName"/>
				</div>
				<div className="form__item">
					Жанры (> genres)
					<PlaySelect allData={allGenres} values={genres} setValues={setGenres}/>
				</div>
				<label className="form__item">
					Описание (description)
					<textarea type="text" value={description} onChange={handleChange(setDescription)} />
				</label>
				<label className="form__item">
					SЕО title (seo_title)
					<input type="text" value={seoTitle} onChange={handleChange(setSeoTitle)}/>
				</label>
				<label className="form__item">
					SЕО description (seo_description)
					<input type="text" value={seoDescription} onChange={handleChange(setSeoDescription)} />
				</label>
				<label className="form__item">
					Ссылка (url)
					<input type="text" value={url} onChange={handleChange(setUrl)} />
				</label>
				
				<div className="actions">
					<button className="button-success pure-button" onClick={save}>Сохранить</button>
					{/*<button className="button-error pure-button">Удалить</button>*/}
				</div>
			</form>
			<ToastContainer/>
		</div>
	);
}

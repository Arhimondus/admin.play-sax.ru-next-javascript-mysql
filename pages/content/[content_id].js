import React, { useEffect, useRef, useState, useCallback } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'lib/axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faTimes } from '@fortawesome/free-solid-svg-icons';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import arrayMove from 'array-move';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PlaySelect from 'lib/play-select';
import { getComposerShortName } from 'lib/utils';
import { access } from 'lib/control';

export let getServerSideProps = access(user => user.admin, async ({ req, res, query: { content_id } }) => {
	const defaultPageNumber = 1;
	const defaultPageSize = 20;
	const defaultSortType = 'composer';

	const { data: content } = await axios.get(`/api/content`, {
		params: { content_id },
	});

	const { data: allInstruments } = await axios.get(`/api/instruments`);
	const { data: allLevels } = await axios.get(`/api/levels`);

	return {
		props: { content, allInstruments, allLevels },
	};
}, '/auth');

function getItemStyle(isDragging, draggableStyle) {
	return {
		// some basic styles to make the items look a bit nicer
		// userSelect: 'none',
		// padding: grid * 2,
		// margin: `0 ${grid}px 0 0`,
		//
		// // change background colour if dragging
		// background: isDragging ? 'lightgreen' : 'grey',

		// styles we need to apply on draggables
		...draggableStyle,
	};
}

function getListStyle() {
	return {
		// background: isDraggingOver ? 'lightblue' : 'lightgrey',
		// display: 'flex',
		// padding: grid,
		// overflow: 'auto',
	};
}

function Score({ tag = 'li', fileName, index, onChange }) {
	const ref = useRef(null);
	const [deleting, setDeleting] = useState(null);

	return <li style={getItemStyle({}, {})} className={['score', deleting ? '--delete' : ''].join(' ')} ref={ref}>
		<a className="score__link" target="__blank"
		   href={`/scores/${fileName}.jpg`}>
			<label href="#delete" className="score__delete">
				<FontAwesomeIcon icon={faTimes} size="2x"
								 color="maroon"/>
				<input type="checkbox" defaultValue={fileName}
					   onChange={({ target: { checked } }) => {
						   setDeleting(checked);
						   onChange(fileName, checked);
					   }}
					   style={{ display: 'none' }}/>
			</label>

			<img className="score__page" src={`/scores/${fileName}.jpg`}/>
		</a>
	</li>;
}

const SortableItem = SortableElement(({ index, fileName, scoresForDelete, setScoresForDelete }) =>
	<Score index={index}
		   onChange={((fileName, deleting) => {
			   if (deleting) {
				   setScoresForDelete([...scoresForDelete, fileName]);
			   } else {
				   setScoresForDelete(scoresForDelete.filter(sfd => sfd !== fileName));
			   }
		   })} fileName={fileName}/>,
);

const SortableList = SortableContainer(({ scores, setScores, scoresForDelete, setScoresForDelete, contentId }) => {
	const newScores = useRef();

	function newScoreChange({ target }) {
		const formData = new FormData();
		console.log('newScores.current.files', target.files);
		Array.from(newScores.current.files).forEach((file, index) => formData.append(`file[${index}]`, file));
		axios.post(`/api/upload-score?content_id=${contentId}`, formData, {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		}).then(({ data }) => {
			console.log('file uploaded');
			console.log('uploaded file names', data);
			setScores([...scores, ...data]);
		}).catch(e => {
			console.log('error');
			console.log(e);
		});
	}

	return <ul className='scores'>
		{scores.map((fileName, index) => <SortableItem key={fileName} index={index} fileName={fileName}
													   scoresForDelete={scoresForDelete}
													   setScoresForDelete={setScoresForDelete}/>)}
		<li className="score-add">
			<label href="#add">
				<FontAwesomeIcon icon={faPlusCircle} size="3x" color="green"/>
				<input type="file" multiple={true} accept="image/jpeg, image/png" onChange={newScoreChange} ref={newScores}
					   style={{ display: 'none' }}/>
			</label>
		</li>
	</ul>;
});

function ScoresList({ scores: [scores, setScores], scoresForDelete: [scoresForDelete, setScoresForDelete], contentId }) {
	function onSortEnd({ oldIndex, newIndex }) {
		setScores(scores => arrayMove(scores, oldIndex, newIndex));
	}

	return <SortableList items={scores} onSortEnd={onSortEnd} axis={'xy'} distance={1} contentId={contentId}
						 setScores={setScores} scores={scores} setScoresForDelete={setScoresForDelete}
						 scoresForDelete={scoresForDelete}/>;
}

export default function Content({ content, allInstruments, allLevels }) {
	const [title, setTitle] = useState(content.title);
	const [url, setUrl] = useState(content.url);
	const [information, setInformation] = useState(content.information);

	const [seoTitle, setSeoTitle] = useState(content.seo_title);
	const [seoDescription, setSeoDescription] = useState(content.seo_description);

	const [type, setType] = useState(0);

	const [downloadLink, setDownloadLink] = useState(content.download_link);
	const [audioFile, setAudioFile] = useState(content.audio_file);
	const [youtubeLink, setYoutubeLink] = useState(content.youtube_link);
	const [youtubeEmbed, setYoutubeEmbed] = useState(content.youtube_embed);

	const [instruments, setInstruments] = useState(content.instruments.map(it => it.id));
	const [levels, setLevels] = useState(content.levels.map(it => it.id));

	const scoresState = useState(content.scores.map(it => it.file_name));
	const scoresForDeleteState = useState([]);

	const [audios, setAudios] = useState(content.audios.map(it => it.file_name));
	const [audiosForDelete, setAudiosForDelete] = useState([]);

	const [_public, setPublic] = useState(content.public);

	function handleChange(setter) {
		return (event) => {
			setter(event.target.value);
		};
	}

	function handleChangeToInt(setter) {
		return (event) => {
			setter(+event.target.value);
		};
	}

	async function saveDraft(evt) {
		await save(evt, true);
	}

	function handleCheckbox(setter) {
		return (event) => {
			setter(event.target.checked);
		};
	}

	async function save(evt, draft = false) {
		evt.preventDefault();
		const scores = scoresState[0].filter(s => !scoresForDeleteState[0].includes(s));
		const _audios = audios.filter(s => !audiosForDelete.includes(s));
		try {
			await axios.post(`/api/save-content`, {
				id: content.id,
				title,
				url,
				information,
				seo_title: seoTitle,
				seo_description: seoDescription,
				type,
				scores,
				download_link: downloadLink,
				audio_file: audioFile,
				youtube_link: youtubeLink,
				youtube_embed: youtubeEmbed,
				instruments,
				levels,
				audios: _audios,
				_public: !draft,
			});
			scoresState[1](scores);
			setAudios(_audios);
			setPublic(!draft);
			toast.success('Изменения сохранены!', {
				position: 'bottom-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
			});
		} catch (err) {
			toast.error(`Ошибка: ${err}`, {
				position: 'bottom-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
			});
		}
	}

	return (
		<div className="main">
			<nav>
				<ul className="menu">
					<li><Link href="/"><a>На главную</a></Link></li>
				</ul>
			</nav>
			<header className="header">
				<h1 className="page-title">{content.title} <span
					className="page-title__sub">{content.note.composers.map(getComposerShortName).join(', ')} - {content.note.title}</span>
					<a className="open-on-site" href={`https://dev.play-sax.ru/ноты/${url}`} target="blank">открыть на
						сайте</a></h1>
				<div className="content-id">
					<span>content id: {content.id}</span>
					<span className="public">опубликован <input type="checkbox" checked={_public} onChange={handleCheckbox(setPublic)}/></span>
					<ul className="datetimes">
						<li className="datetime">создание: {new Date(content.created).toLocaleString('ru-RU', { timeZone: 'UTC' })}</li>
						<li className="datetime">модификация: {new Date(content.modified).toLocaleString('ru-RU', { timeZone: 'UTC' })}</li>
					</ul>
				</div>
			</header>
			<form className="form">
				<label className="form__item">
					Название (title)
					<input type="text" value={title} onChange={handleChange(setTitle)}/>
				</label>
				<div className="form__item">
					Инструменты (> instruments) {JSON.stringify(instruments)}
					<PlaySelect allData={allInstruments} values={instruments} setValues={setInstruments}/>
				</div>
				<div className="form__item">
					Уровни (> levels) {JSON.stringify(levels)}
					<PlaySelect allData={allLevels} values={levels} setValues={setLevels}/>
				</div>
				<label className="form__item">
					Ссылка (url)
					<input type="text" value={url} onChange={handleChange(setUrl)}/>
				</label>
				<label className="form__item">
					Доп. информация (information)
					<textarea type="text" defaultValue={information} onChange={handleChange(setInformation)}/>
				</label>
				<label className="form__item">
					SЕО title (seo_title)
					<input type="text" value={seoTitle} onChange={handleChange(setSeoTitle)}/>
				</label>
				<label className="form__item">
					SЕО description (seo_description)
					<textarea type="text" defaultValue={seoDescription} onChange={handleChange(setSeoDescription)}/>
				</label>
				<label className="form__item">
					Тип (type)
					<select value={type} onChange={handleChangeToInt(setType)}>
						<option value={0}>Ноты</option>
						<option value={1}>Минус</option>
					</select>
				</label>
				<div className="form__item">
					Ноты ({scoresState[0].length})
					{/*<p>{JSON.stringify(scoresState[0])}</p>
					<p>{JSON.stringify(scoresForDeleteState[0])}</p>
					<p>{JSON.stringify(scoresState[0].filter(s => !scoresForDeleteState[0].includes(s)))}</p>*/}
					<ScoresList scores={scoresState} scoresForDelete={scoresForDeleteState} contentId={content.id}/>
				</div>
				<label className="form__item" title="Обычно на Яндекс.Диск или другой файлообменник">
					Ссылка на скачивание (download_link)
					<input type="text" value={downloadLink} onChange={handleChange(setDownloadLink)}/>
				</label>
				<div className="form__item" title="Название файла в папке audio на сервере">
					Аудиофайлы (audio_file)
					<ul style={{ listStyle: 'none', padding: 0 }}>
						{audios.map(audio => <li key={`audio-${audio}`}
												 className={['mini-player', 'audio', audiosForDelete.includes(audio) ? '--delete' : ''].join(' ')}>
							{
								audio
									?
									<>
										<audio className="audio__player" controls src={`/audios/${audio}.mp3`}
											   title={audio}/>
										<label href="#delete" className="audio__delete">
											<FontAwesomeIcon icon={faTimes} size="1x"
															 color="maroon"/>
											<input type="checkbox" defaultValue={audio}
												   onChange={({ target: { checked } }) => {
													   if (checked) {
														   setAudiosForDelete([...audiosForDelete, audio]);
													   } else {
														   setAudiosForDelete(audiosForDelete.filter(it => it !== audio));
													   }
												   }}
												   style={{ display: 'none' }}/>
										</label>
									</>
									: 'Аудио не загружено'
							}
						</li>)}
					</ul>
					<input type="file" accept="audio/mp3" onChange={({ target }) => {
						const formData = new FormData();
						console.log('newAudios.current.files', target.files);
						Array.from(target.files).forEach((file, index) => formData.append(`file[${index}]`, file));
						axios.post(`/api/upload-audio?content_id=${content.id}`, formData, {
							headers: {
								'Content-Type': 'multipart/form-data',
							},
						}).then(({ data }) => {
							console.log('file uploaded');
							console.log('uploaded file names', data);
							setAudios([...audios, data[0]]);
							target.value = null;
						}).catch(e => {
							console.log('error');
							console.log(e);
						});
					}}/>
				</div>
				<label className="form__item">
					Ссылка на ютуб видео (youtube_link)
					<input type="text" value={youtubeLink} onChange={handleChange(setYoutubeLink)}/>
				</label>
				<label className="form__item">
					Ссылка на ютуб видео (для встраивания) (youtube_embed)
					<input type="text" value={youtubeEmbed} onChange={handleChange(setYoutubeEmbed)}/>
				</label>
				<div className="actions">
					{_public
						? <button className="button-success pure-button" onClick={save}>Сохранить</button>
						: <>
							<button className="button-success pure-button" onClick={save}>Опубликовать</button>
							<button className="button-info pure-button" onClick={saveDraft}>Сохранить</button>
						</>
					}
				</div>
			</form>
			<ToastContainer/>
		</div>
	);
}

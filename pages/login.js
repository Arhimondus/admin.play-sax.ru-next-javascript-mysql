import React from 'react';
import { access, getUser } from 'lib/control';

export const getServerSideProps = access(user => true, '/profile');

export default function NotAuthorized() {
	return <div>
		<h1>Вы не авторизованы!</h1>
		<fieldset style={{ width: '250px' }}>
			<legend>Логин-пароль</legend>
			<form method="GET" action="/api/auth/login-password">
				<input type="text" name="login" placeholder="Имя пользователя"/>
				<input type="password" name="password" placeholder="Пароль"/>
				<input type="submit" value="Войти"/>
			</form>
		</fieldset>
	</div>;
};
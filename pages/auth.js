import React from 'react';
import { getUser, access } from 'lib/control';

export let getServerSideProps = access(user => true, (req, res) => {
	return {
		props: {
			user: req.user,
		},
	};
});

export default function Page({ user }) {
	return <>
		<h1>Запретная зона</h1>
		<p>Вы находитесь в запретной зоне! Любое неправильное движение может привести к печальным последствиям. Вы действительно хотите продолжить?</p>
		{!user && <>
			Не авторизован <br/>
			<a href="/login">Войти</a>
		</>}
		{user && <>
			{JSON.stringify(user)}
			Авторизован как {user?.name} <br/>
			{user.admin ? 'Добрый день, администратор!' : 'Вы простой смертный'}
			<a href="/api/auth/logout">Выйти</a>
		</>}
	</>;
};
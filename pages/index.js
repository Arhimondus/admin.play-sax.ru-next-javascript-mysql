import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'lib/axios';
import { getComposerShortName } from 'lib/utils';
import { access } from 'lib/control';

export let getServerSideProps = access(user => user.admin, async ({ req, res }) => {
	const defaultPageNumber = 1;
    const defaultPageSize = 20;
    const defaultSortType = 'composer';

    const { data: notes } = await axios.get(`/api/all-contents`, {
        params: {
            pageNumber: defaultPageNumber, pageSize: defaultPageSize, composerIds: [], levelIds: [],
            sortType: defaultSortType,
        },
    });

    return {
        props: { notes },
    };
}, '/auth');

export default function Notes({ notes }) {
    return (
        <>
            <header className="header">
                <h1 className="page-title">Панель администрирования - список произведений</h1>
                <a className="add-content button-secondary pure-button" href={`/api/add-note`}>Добавить произведение</a>
            </header>
            <table className="kovalsky-table pure-table pure-table-striped">
                <thead>
                <tr>
                    <th>
                        Произведение
                    </th>
                    <th>
                        Контенты
                    </th>
                </tr>
                </thead>
                <tbody>
                {notes.map(note => <tr className="notes-content" key={`note-${note.id}`}>
                    <td className="notes-content__titleColumn">
                        <Link href={'/note/' + note.id}>
                            <a>{note.composers.map(getComposerShortName).join(', ') || '<Без композитора>'} - {note.title || '<Без имени>'}</a>
                        </Link>
                    </td>
                    <td className="notes-content__linksColumn">
                        {note.contents.map((content, index) => <div key={`content-${index}`} className={['content', !content.public ? '--draft' : ''].join(' ')}>
                            <Link href={'/content/' + content.id}>
                                <a className="content__link">{content.title || '<Без имени>'}</a>
                            </Link>
                            <div className="content__instruments">{
                                content.instruments.length > 0
                                    ? content.instruments.map(it => it.title).join(', ')
                                    : <span style={{backgroundColor: '#f6dede'}}>Не выбран инструмент</span>
                            }</div>
                        </div>)}
                        <a className="add-content button-secondary pure-button"
                           href={`/api/add-content?note_id=${note.id}`}>Добавить контент</a>
                    </td>
                </tr>)}
                {/*<tr>*/}
                {/*	<td colSpan={3}>*/}
                {/*		<div>Страница {pageNumber}</div>*/}
                {/*		<button onClick={() => setPageNumber(pageNumber => pageNumber + 1)}>Следующая страница</button>*/}
                {/*	</td>*/}
                {/*</tr>*/}
                </tbody>
            </table>
        </>
    );
}

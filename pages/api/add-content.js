import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const { note_id } = req.query;

		const [{ insertId }] = await pool.query(`INSERT INTO contents SET ?`, { note_id });

		res.redirect(`/content/${insertId}`);
	} catch (err) {
		res.status(400).send(err);
	}
}
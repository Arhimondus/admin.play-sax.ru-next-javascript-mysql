import pool from 'lib/db';
import fs from 'fs-extra';
import { IncomingForm } from 'formidable';
import short from 'short-uuid';
import changeJpegMeta from 'lib/changeJpegMeta';
import dirname from 'lib/dirname';
import { join } from 'path';

export const config = {
	api: {
		bodyParser: false,
	},
};

function parse(req) {
	return new Promise((resolve, reject) => {
		try {
			const form = new IncomingForm();

			form.parse(req, (err, fields, files) => {
				if(err) {
					return reject(err);
				}
				resolve({ fields, files });
			});
		} catch(err) {
			reject(err);
		}
	});
}

async function processFile({ path }, content_id) {
	const [scores] = await pool.query('SELECT * FROM content_scores WHERE content_id = ?', content_id);
	const [[{ title, note_title, composers }]] = await pool.query(`SELECT
			contents.title, 
			notes.title AS note_title,
			composers.familiya as composers
		FROM contents
			LEFT JOIN notes ON notes.id = contents.note_id
			LEFT JOIN note_genres ON notes.id = note_genres.note_id
			LEFT JOIN genres ON genres.id = note_genres.genre_id
			LEFT JOIN content_instruments ON contents.id = content_instruments.content_id
			LEFT JOIN instruments ON content_instruments.instrument_id = instruments.id
			LEFT JOIN note_composers ON notes.id = note_composers.note_id
			LEFT JOIN content_levels ON notes.id = content_levels.content_id 
			LEFT JOIN composers ON note_composers.composer_id = composers.id
			LEFT JOIN levels ON content_levels.level_id = levels.id
		WHERE contents.id = ? LIMIT 1`, content_id);

	let order_id;

	if(!scores || scores.length === 0) {
		order_id = 1;
	} else {
		const sorted = scores.sort((a, b) => a.order_id - b.order_id);
		order_id = sorted[sorted.length - 1].order_id + 1;
	}

	const file_name = short.generate();

	console.log('planning to insert into table', content_id, order_id, file_name);

	await pool.query('INSERT INTO content_scores SET ?', { content_id, order_id, file_name });

	const destPath = `./public/scores/${file_name}.jpg`;
	
	await fs.move(path, destPath, { overwrite: true });

	console.log('dirname', dirname);
	console.log('changeJpegMeta', `${note_title} (${title})`, composers, content_id);

	await changeJpegMeta(join(dirname, destPath), `${note_title} (${title})`, composers, content_id);

	return file_name;
}

export default async function handler(req, res) {
	try {
		const { content_id } = req.query;

		console.log(content_id);

		const { fields, files } = await parse(req);

		console.log('fields', fields);
		console.log('files', files);

		const fileNames = await Promise.all(Object.keys(files).map(async key => processFile(files[key], content_id)));
		console.log('fileNames', fileNames);

		res.send(fileNames);
	} catch(ex) {
		console.log(ex);
	}
};
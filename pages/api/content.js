import sequelize from 'lib/db';
import Content from 'models/content';
import Serializer from 'sequelize-to-json';
import Composer from 'models/composer';
import Note from 'models/note';
import Instrument from 'models/instrument';
import Level from 'models/level';
import Audio from 'models/content_audio';
import Score from 'models/content_score';

export default async function handler(req, res) {	
    try {
        const {content_id} = req.query;
        const data = await Content.findOne({
            where: {
                id: content_id,
            },
            order: [
                [{ model: Score, as: 'scores' }, 'order_id', 'asc'],
            ],
            include: [
                {
                    model: Instrument,
                    as: 'instruments',
                },
                {
                    model: Level,
                    as: 'levels',
                },
                {
                    model: Audio,
                    as: 'audios',
                },
                {
                    model: Score,
                    as: 'scores',
                },
                {
                    model: Note,
                    as: 'note',
                    include: {
                        model: Composer,
                        as: 'composers',
                    },
                },
            ],
        });

        console.log('content T$!O@$', data);

        res.json(data.serialize({
            include: [
                '@all',
                'instruments',
                'levels',
                'audios',
                'scores',
                'note',
            ],
            assoc: {
                instruments: {},
                levels: {},
                audios: {},
                scores: {},
                note: {
                    include: ['@all', 'composers'],
                    assoc: {
                        composers: {},
                    },
                },
            },
        }));
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}
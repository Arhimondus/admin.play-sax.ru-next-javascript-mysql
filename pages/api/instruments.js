import pool from 'lib/db';
import Instrument from "models/instrument";

export default async function handler(req, res) {	
	try {
		const composers = await Instrument.findAll();
		return res.json(composers.serialize());
	} catch (err) {
		res.status(400).send(err);
	}
}
import pool from 'lib/db';
import Note from "models/note";
import Composer from "models/composer";
import Genre from "models/genre";
import Content from "models/content";

export default async function handler(req, res) {	
    try {
        const {note_id} = req.query;

        const note = await Note.findOne({
            where: {
                id: note_id,
            },
            include: [
                {
                    model: Composer,
                    as: 'composers',
                },
                {
                    model: Genre,
                    as: 'genres',
                },
                {
                    model: Content,
                    as: 'contents',
                },
            ],
        });

        res.json(note.serialize({
            include: ['@all', 'composers', 'genres', 'contents'],
            assoc: {
                composers: {},
                genres: {},
                contents: {},
            },
        }));
    } catch (err) {
        res.status(400).send(err);
    }
}
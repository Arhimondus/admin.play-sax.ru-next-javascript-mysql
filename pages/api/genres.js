import pool from 'lib/db';
import Genre from "models/genre";

export default async function handler(req, res) {	
	try {
		const genres = await Genre.findAll();
		return res.json(genres.serialize());
	} catch (err) {
		res.status(400).send(err);
	}
}
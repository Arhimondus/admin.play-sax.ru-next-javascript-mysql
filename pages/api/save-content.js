import pool, { linkedValuesQuery } from 'lib/db';

export default async function handler(req, res) {
	try {		
		const {
			id,
			title,
			url,
			information,
			seo_title,
			seo_description,
			type,
			scores,
			download_link,
			audio_file,
			youtube_link,
			youtube_embed,
			instruments,
			audios,
			levels,
			_public,
		} = req.body;

		await linkedValuesQuery('content_scores', scores, 'content_id', id, 'file_name', true);
		await linkedValuesQuery('content_levels', levels, 'content_id', id, 'level_id');
		await linkedValuesQuery('content_instruments', instruments, 'content_id', id, 'instrument_id');
		await linkedValuesQuery('content_audios', audios, 'content_id', id, 'file_name');

		await pool.query('UPDATE contents SET ? WHERE id = ?', [{
			id,
			title,
			url,
			information,
			seo_title,
			seo_description,
			type,
			download_link,
			audio_file,
			youtube_link,
			youtube_embed,
			public: _public,
		}, id]);
		
		res.json(true);
	} catch (err) {
		console.log(err);
		res.status(400).send(err);
	}
}
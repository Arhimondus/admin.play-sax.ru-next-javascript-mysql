import pool from 'lib/db';
import Composer from "models/composer";

export default async function handler(req, res) {
	try {
		const composers = await Composer.findAll();
		return res.json(composers.serialize());
	} catch (err) {
		res.status(400).send(err);
	}
}
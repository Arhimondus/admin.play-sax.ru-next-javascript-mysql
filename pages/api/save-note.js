import pool, { linkedValuesQuery } from 'lib/db';

export default async function handler(req, res) {	
	try {
		const {
			id,
			title,
			description,
			seo_title,
			seo_description,
			url,
			composers,
			genres,
		} = req.body;
			
		console.log('body', req.body);

		await pool.query('UPDATE notes SET ? WHERE id = ?', [{
			id,
			title,
			description,
			seo_title,
			seo_description,
			url,
		}, id]);

		await linkedValuesQuery('note_composers', composers, 'note_id', id, 'composer_id');
		await linkedValuesQuery('note_genres', genres, 'note_id', id, 'genre_id');
		
		res.json(true);
	} catch (err) {
		console.log(err);
		res.status(400).send(err);
	}
}
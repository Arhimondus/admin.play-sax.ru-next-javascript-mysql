import pool from 'lib/db';

export default async function handler(req, res) {
	try {
		const [{ insertId }] = await pool.query(`INSERT INTO notes() VALUES()`);

		res.redirect(`/note/${insertId}`);
	} catch (err) {
		res.status(400).send(err);
	}
}
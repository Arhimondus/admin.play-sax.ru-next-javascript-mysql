import { control } from 'lib/control';

export default control((req, res) => {
	console.log('trying to logout!');
	req.logout();
	res.redirect('/login');
});
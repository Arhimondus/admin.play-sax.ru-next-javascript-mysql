import pool from 'lib/db';
import fs from 'fs-extra';
import { IncomingForm } from 'formidable';
import short from 'short-uuid';
import changeJpegMeta from 'lib/changeJpegMeta';
import { join } from 'path';

export const config = {
	api: {
		bodyParser: false,
	},
};

function parse(req) {
	return new Promise((resolve, reject) => {
		try {
			const form = new IncomingForm();

			form.parse(req, (err, fields, files) => {
				if(err) {
					return reject(err);
				}
				resolve({ fields, files });
			});
		} catch(err) {
			reject(err);
		}
	});
}

async function processFile({ path }, content_id) {
	const [audios] = await pool.query('SELECT * FROM content_audios WHERE content_id = ?', content_id);

	let order_id;

	if(!audios || audios.length === 0) {
		order_id = 0;
	} else {
		const sorted = audios.sort((a, b) => a.order_id - b.order_id);
		order_id = sorted[sorted.length - 1].order_id + 1;
	}

	const file_name = short.generate();

	console.log('planning to insert into table', content_id, order_id, file_name);

	await pool.query('INSERT INTO content_audios SET ?', { content_id, order_id, file_name });

	const destPath = `./public/audios/${file_name}.mp3`;
	
	await fs.move(path, destPath, { overwrite: true });

	return file_name;
}

export default async function handler(req, res) {
	const { content_id } = req.query;

	console.log(content_id);

	const { fields, files } = await parse(req);

	console.log('fields', fields);
	console.log('files', files);

	const fileNames = await Promise.all(Object.keys(files).map(async key => processFile(files[key], content_id)));
	console.log('fileNames', fileNames);

	res.send(fileNames);
};
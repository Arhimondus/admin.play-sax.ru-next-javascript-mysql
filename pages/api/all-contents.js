import pool from 'lib/db';
import Content from "models/content";
import Instrument from "models/instrument";
import Audio from "models/content_audio";
import Score from "models/content_score";
import Note from "models/note";
import Composer from "models/composer";
import Serializer from "sequelize-to-json";
import Level from 'models/level';

export default async function handler(req, res) {	
	try {
		const { pageNumber = 1, pageSize = 20, composerIds, levelIds, sortType } = req.query;

		const contents = await Note.findAll({
			include: [
				{
					model: Composer,
					as: 'composers',
				},
				{
					model: Content,
					as: 'contents',
					order: [
						['order_id', 'ASC'],
					],
					include: [
						{
							model: Instrument,
							as: 'instruments',
						},
						{
							model: Level,
							as: 'levels',
						},
					],
				},
			],
		});

		console.log('contents', contents);

		res.json(contents.serialize({
			include: ['@all', 'composers', 'contents'],
			assoc: {
				composers: {},
				contents: {
					include: ['@all', 'instruments', 'levels'],
					assoc: {
						instruments: {},
						levels: {},
					},
				},
			},
		}));
	} catch (err) {
		console.log(err);
		res.status(400).send(err);
	}
}
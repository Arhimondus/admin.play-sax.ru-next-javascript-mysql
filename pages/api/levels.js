import pool from 'lib/db';
import Level from "models/level";

export default async function handler(req, res) {
	try {
		const levels = await Level.findAll();
		return res.json(levels.serialize());
	} catch (err) {
		res.status(400).send(err);
	}
}
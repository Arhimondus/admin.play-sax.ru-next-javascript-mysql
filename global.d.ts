interface Composer {
    id: number;
    imya: string;
    familiya: string;
    otchestvo: string;
}

interface Content {
    id: number;
    note_id: number;
    order_id: number;
    url: string;
    title: string;
    information: text;
    seo_title: string;
    seo_description: string;
    type: number;
    pages_count: number;
    download_link: string;
    download_file: string;
    music_xml: string;
    audio_file: string;
    video_file: string;
    youtube_link: string;
    youtube_embed: string;
    note?: Note;
}

interface Note {
    id: number;
    title: string;
    description: string;
    seo_title: string;
    seo_description: string;
    url: string;
    contents?: Array<Content>;
    composers?: Array<Composer>;
    genres?: Array<Genre>;
}

interface Genre {
    id: number;
    title: string;
    url: string;
}

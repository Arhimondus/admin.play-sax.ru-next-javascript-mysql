// next.config.js
module.exports = {
	serverRuntimeConfig: {
		host: process.env.HOST,
		port: process.env.PORT,
		url: process.env.LOCAL_URL,
		user: process.env.USER,
		database: process.env.DATABASE,
		password: process.env.PASSWORD,
		port: process.env.PORT,
		secretCookiePassword: process.env.SECRET_COOKIE_PASSWORD,
		vkAuthClientId: process.env.VK_AUTH_CLIENT_ID,
		vkAuthSecret: process.env.VK_AUTH_SECRET,
		vkAuthCallback: process.env.VK_AUTH_CALLBACK_URL,
		vkAuthScope: process.env.VK_AUTH_SCOPE.split(' '),
		vkAuthProfileFields: process.env.VK_AUTH_PROFILE_FIELDS.split(' '),
	},
	publicRuntimeConfig: {
		port: process.env.PUBLIC_PORT,
		url: process.env.PUBLIC_URL,
	},
};
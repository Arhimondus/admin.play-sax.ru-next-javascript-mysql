import passportControl from 'next-passport-control';
import getConfig from 'next/config';
import { Strategy } from 'passport-custom';
import { Zeuvs } from 'auth-vk';
import dynamic from 'next/dynamic';
const { serverRuntimeConfig } = getConfig();
import connectRedis from 'connect-redis';
import redis from 'redis';

const { auth, access, control, getUser, passport } = passportControl(session => {
	let RedisStore = connectRedis(session);
	let redisClient = redis.createClient();
	return {
		secret: serverRuntimeConfig.secretCookiePassword,
		resave: true,
		saveUninitialized: true,
		store: new RedisStore({ client: redisClient }),
	};
});
	
if(!process.browser) {
	console.log('CONTROL RUNNING FIRST TIME!');

	passport.serializeUser(function (user, done) {
		done(null, user);
	});

	passport.deserializeUser(function (obj, done) {
		done(null, obj);
	});

	passport.use('login-password', new Strategy(
		async (req, done) => {
			const { login, password } = req.query;
			const user = (login == 'evg' && password == process.env.TEMPORARY_ADMIN_PASSWORD) ? { id: 1, name: 'Евгений', admin: true } : null;
			
			if(!user) {
				return done(null, false);
			}
			
			done(null, {
				id: user.id,
				name: user.name,
				admin: user.admin,
			});
		}
	));

	passport.use('vkontakte', new auth({
		clientID: serverRuntimeConfig.vkAuthClientId,
		clientSecret: serverRuntimeConfig.vkAuthSecret,
		callbackURL: serverRuntimeConfig.vkAuthCallback,
		scope: serverRuntimeConfig.vkAuthScope,
		profileFields: serverRuntimeConfig.vkAuthProfileFields,
	},
	async function verify(accessToken, refreshToken, params, { id: vk_id, displayName: vk_name, email: vk_email }, done) {
		process.nextTick(async function () {
			await pool.beginTransaction();
			const [[account]] = await pool.query(`SELECT user_id FROM accounts WHERE provider_id = 'vk' AND provider_account_id = ?`, vk_id);
			let user_id;
			let user_name;
			let user_admin;
			if(account) {
				user_id = account.user_id;
				const [[user]] = await pool.query(`SELECT name, admin FROM users WHERE user_id = ?`, user_id);
				user_name = user.name;
				user_admin = user.admin;
			} else {
				const { insertId } = await pool.query(`INSERT INTO users (name, vk_email) VALUES (?, ?)`, [vk_name, vk_email]);
				user_id = insertId;
				await pool.query(`INSERT INTO accounts (provider_id, provider_account_id, user_id) VALUES ('vk', ?, ?)`, [vk_id, user_id]);
				user_name = vk_name;
				user_admin = false;
			}
			await connection.commit();
			await connection.end();
			
			done(null, {
				id: user_id,
				name: user_name,
				admin: user_admin,
			});
		});
	}));
}

export {
	auth,
	control,
	access,
	getUser,
}
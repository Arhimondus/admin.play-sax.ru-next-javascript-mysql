import React from 'react';
import MultiSelect from 'react-multi-select-component';

export default function PlaySelect({ allData, values, setValues, dataFieldValue = 'id', dataFieldLabel = 'title' }) {
	const options = allData.map(it => ({
		value: it[dataFieldValue],
		label: it[dataFieldLabel]
	}));
	return <MultiSelect
		options={options}
		value={options.filter(it => values.includes(it.value))}
		onChange={(values) => setValues(values.map(it => it.value))}
		overrideStrings={{
			"allItemsAreSelected": "Все элементы выбраны",
			"clearSearch": "Очистить поиск",
			"noOptions": "Нет опций",
			"search": "Поиск",
			"selectAll": "Выбрать всё",
			"selectSomeItems": "Выбрать..."
		}}
		hasSelectAll={false}
	/>
}
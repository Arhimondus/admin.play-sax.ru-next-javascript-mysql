import axios from 'axios';
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

const baseURL = !process.browser ? `${serverRuntimeConfig.url}:${serverRuntimeConfig.port}` : `${publicRuntimeConfig.url}:${publicRuntimeConfig.port}`;

export default axios.create({
	baseURL,
});
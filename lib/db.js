import mysql from 'mysql2/promise';
import { Model, Sequelize } from 'sequelize';
import initModels from 'models/init-models';
import Serializer from 'sequelize-to-json';
import Composer from "models/composer";
import Note from "models/note";

const pool = mysql.createPool({
	host: process.env.HOST,
	user: process.env.USER,
	database: process.env.DATABASE,
	password: process.env.PASSWORD,
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
});

const sequelize = new Sequelize(process.env.DATABASE, process.env.USER, process.env.PASSWORD, {
	host: process.env.HOST,
	dialect: 'mysql',
});

Model.prototype.serialize = function(scheme, options) {
	return (new Serializer(this.constructor, scheme, options)).serialize(this);
}

Array.prototype.serialize = function(scheme, options) {
	if(this.length > 0) {
		return Serializer.serializeMany(this, this[0].constructor, scheme, options);
	} else {
		return [];
	}
}

export default pool;

async function linkedValuesQuery(table, values, primaryField, primaryId, idField, setOrderId = false) {
	const alreadyData = (await pool.query(`SELECT ${idField} FROM ${table} WHERE ${primaryField} = ?`, primaryId))[0].map(s => s[idField]);
	const dataForDelete = alreadyData.filter(s => !values.includes(s));
	const dataForInsert = values.filter(v => !alreadyData.includes(v));

	if (dataForDelete.length > 0) {
		await pool.query(`DELETE FROM ${table} WHERE ${idField} IN (${new Array(dataForDelete.length).fill('?')})`, dataForDelete);
	}

	if (dataForInsert.length > 0) {
		console.log(3);
		for (let item of dataForInsert) {
			await pool.query(`INSERT INTO ${table} SET ?`, { [primaryField]: primaryId, [idField]: item });
		}
	}

	if (setOrderId && values.length > 0) {
		for (let index = 0; index < values.length; index++) {
			const item = values[index];
			await pool.query(`UPDATE ${table} SET order_id = ? WHERE ${idField} = ?`, [index, item]);
		}
	}
}

initModels(sequelize);

export {
	linkedValuesQuery,
	sequelize,
};
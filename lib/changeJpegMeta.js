import piexif from 'piexifjs';
import fs from 'fs-extra';
import pngToJpeg from 'png-to-jpeg';
import imageType from 'image-type';
import readChunk from 'read-chunk';

function getFileData(path) {
	return new Promise((resolve, reject) => {
		const type = imageType(readChunk.sync(path, 0, 12));
		console.log('imageType', type);
		if(type.mime == 'image/png') {
			console.log('vetka!');
			const buffer = fs.readFileSync(path);
			const output = pngToJpeg({ quality: 95 })(buffer).then(output => {
				console.log('output', output);
				resolve(output);
			});
		} else if(type.mime == 'image/jpeg') {
			resolve(fs.readFileSync(path));
		} else {
			reject('Not supported type!');
		}
	});
}

export default async function(path, title, author, content_id) {	
	console.log('my path', path);

	const jpeg = await getFileData(path);
	const file = jpeg.toString('binary');

	const zeroth = {};
	zeroth[piexif.ImageIFD.XPTitle] = [...Buffer.from(title, 'ucs2')];
	zeroth[piexif.ImageIFD.ImageNumber] = +content_id;
	zeroth[piexif.ImageIFD.XPAuthor] = [...Buffer.from(author, 'ucs2')];

	const exif = {};
	exif[piexif.ExifIFD.ImageUniqueID] = +content_id;

	const exifObj = { "0th": zeroth, "Exif": exif };

	console.log(exifObj);

	const exifbytes = piexif.dump(exifObj);
	const newData = piexif.insert(exifbytes, file);
	const newJpeg = new Buffer(newData, 'binary');

	fs.writeFileSync(path, newJpeg);
}
function getComposerShortName(composer) {
    if(composer.familiya && composer.imya && composer.otchestvo) {
        return `${composer.imya.slice(0, 1)}. ${composer.otchestvo.slice(0, 1)}. ${composer.familiya}.`;
    } else if(composer.familiya && composer.imya) {
        return `${composer.imya} ${composer.familiya}`;
    } else if(composer.imya) {
        return `${composer.imya}`;
    } else if(composer.familiya) {
        return `${composer.familiya}`;
    } else {
        return `<Безымянный композитор>`;
    }
}

export {
    getComposerShortName,
}